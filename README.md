# Sopes 2 - Proyecto

## **Kubernetes**

comandos:
back
```
    1. docker build -t gcr.io/valid-tuner-323400/aydrive-back .
    2. docker push gcr.io/valid-tuner-323400/aydrive-back
    3. kubectl apply -f deployment-back.yaml
    4. kubectl apply -f service-back.yaml
```

luego de levantar el back podemos ver la ip del load balancer

```
    kubectl get services
```

la ip que nos de el load balancer del back habra que ponerla en el front-end para hacer peticiones.

front
```
    1. docker build -t gcr.io/valid-tuner-323400/aydrive-front .
    2. docker push gcr.io/valid-tuner-323400/aydrive-front
    3. kubectl apply -f deployment-front.yaml
    4. kubectl apply -f service-front.yaml
```
## **Rutas**
- ARCHIVO_URL=http://localhost:5000/archivo
- MICROSERVICE_CARPETA_URL=http://localhost:8000/api/carpeta
- MICROSERVICE_USUARIO_URL=http://localhost:9000/user
- MICROSERVICE_BACKLOG_URL=http://localhost:4000/mid
- USUARIO_URL=http://localhost:8080/usuario

---

# **Usuarios**
## *Endpoints*

**Login Usuario:** *POST* - ```http://[ip/localhost]:9000/user/login```

* Entrada
```json
    {
        "usuario": String,
        "contrasena": String,
    }
```
* Salida
```json
    {
        "mensaje": String,
        "estado": Int(Ok = json con datos del usuario, 400 = Error usuario no encontrado)
    }
```
**Registro Usuario:** *POST* - ```http://[ip/localhost]:9000/user/crear```

* Entrada
```json
    {
        "usuario": String,
        "nombre":String,
        "fecha_nacimiento":String,
        "correo":String,
        "contrasena": String,
        "contrasena2": String,
        "numero":String
    }
```
* Salida
```json
    {
        "mensaje": String,
        "estado": Int(Ok = id_user, 400 = Nombre de usuario ya existe, 401 = Correo ya en uso, message: Error)
    }
```

**Editar Usuario:** *POST* - ```http://[ip/localhost]:9000/user/editar```

* Entrada
```json
    {
        "id_user": String,
        "nombre":String,
        "correo":String,
        "fecha_nacimiento": String,
        "contrasena": String,
    }
```
* Salida
```json
    {
        "mensaje": String,
        "estado": Int(200 = Ok, 400 = Error al obtener el usuario de la bd, 401 = Error al actualizar la info en la bd, 403 = Error, el usuario no existe)
    }
```

**Eliminar Usuario:** *POST* - ```http://[ip/localhost]:9000/user/eliminar```

* Entrada
```json
    {
        "id_user": String
    }
```
* Salida
```json
    {
        "mensaje": String,
        "estado": Int(200 = Ok, 400 = No se encuentra usuario, 403 = Usuario no existe, 401 = Error al eliminar)
    }
```

**Autenticacion de Usuario:** *POST* - ```http://[ip/localhost]:9000/user/autenticar```

* Entrada
```json
    {
        "id_user": String,
        "secret": String,
        "tipo": Int (0 = Autenticacion solo por EMAIL, 1 = Autenticacion solo por SMS, 2 = Autenticacion por EMAIL Y SMS)

    }
```
* Salida
```json
    {
        "mensaje": String,
        "token" : String,
        "estado": Int(200 = Ok, 100 = OK, 400 = No se encuentra usuario, 403 = Usuario no existe, 500 = Error al enviar el correo, 402 = Error al enviar el SMS)
    }
```

**Validar Token de Usuario:** *POST* - ```http://[ip/localhost]:9000/user/validarToken```

* Entrada
```json
    {
        "secret": String,
        "token" : "String"
    }
```
* Salida
```json
    {
        "mensaje": String,
        "token_valido": Boolean(true = Ok, false = no valido)
    }
```

---

# **Carpetas**
## *Endpoints*

**Crear carpeta:** *POST* - ```http://[ip/localhost]:8000/api/carpeta/create```

* Entrada
```json
    {
        "id_user": String,
        "fecha_creacion": String,
        "nombre": String,
        "id_padre": String,
    }
```

* Salida
```json
    {
        "mensaje": String,
        "estado": Int(200 = Ok, 403 = Usuario no encontrado, 404 = Ruta no encontrada, 400 = DB Fallo, 402 = Carpeta duplicada),
    }
```

**Eliminar carpeta:** *DELETE* - ```http://[ip/localhost]:8000/api/carpeta/delete```

* Entrada
```json
    {
        "id_user": String,
        "id_carpeta": String,
    }
```

* Salida
```json
    {
        "mensaje": String,
        "estado": Int(200 = Ok, 403 = Usuario no encontrado, 404 = Carpeta no encontrada, 400 = DB Fallo),
    }
```

**Ver root carpetas:** *POST* - ```http://[ip/localhost]:8000/api/carpeta/root```

* Entrada
```json
    {
        "id_user": String
    }
```

* Salida
```json
    {
        "mensaje": String,
        "carpetas": [{
            "id": String,
            "nombre": String,
            "fecha_creacion": String,
            "carpetas": [],
            "archivos": []
        }]
        "estado": Int(200 = Ok, 403 = Usuario no encontrado, 400 = DB Fallo)
    }
```

**Ver carpeta por id:** *POST* - ```http://[ip/localhost]:8000/api/carpeta/read```

* Entrada
```json
    {
        "id_user": String,
        "id_carpeta": String
    }
```

* Salida
```json
    {
        "mensaje": String,
        "carpetas": [{
            "id": String,
            "nombre": String,
            "fecha_creacion": String,
            "carpetas": [],
            "archivos": []
        }]
        "estado": Int(200 = Ok, 403 = Usuario no encontrado, 404 = Carpeta no encontrada, 400 = DB Fallo),
    }
```

**Actualizar carpeta id:** *POST* - ```http://[ip/localhost]:8000/api/carpeta/3000/update```

* Entrada
```json
    {
        "id_user": String,
        "id_carpeta": String,
        "nombre": String
    }
```

* Salida
```json
    {
        "mensaje": String,
        "estado": Int(200 = Ok, 403 = Usuario no encontrado, 404 = Carpeta no encontrada, 400 = DB Fallo, 402 = Carpeta duplicada)
    }
```
---

# **Archivos**
## *Endpoints*

**Crear Archivo:** *POST* - ```http://[ip/localhost]localhost:5000/archivo/crearArchivo```

* Entrada
```json
    {
        "id_user": String,
        "id_carpeta": String,
        "nombre_archivo": String,
        "ext":String,
        "archivo":String
    }
```
* NOTA_1: Ni el campo nombre_archivo, ni el campo ext, deben contener un punto "." .

* NOTA_2: El campo archivo corresponde al archivo en tipo string de su respectiva base64, sin importar si son archivos tipo imagen, texto plano, office, etc.

* Salida
```json
    {
        "mensaje": String,
        "estado": Int(200 = Ok, 400 = No se pudo obtener usuario de la bd, 403 = Usuario no existe, 400 = No se pudo actualizar la informacion de la bd)
    }
```

**Ver Archivo:** *POST* - ```http://[ip/localhost]:5000/archivo/ver```

* Entrada
```json
    {
        "id_user": String,
        "id_carpeta": String,
        "id_archivo": String
    }
```

* Salida
```json
    {
        "mensaje": String,
        "estado": Int(200 = Ok, 400 = No se pudo obtener usuario de la bd, 403 = Usuario no existe, 401 = Archivo no encontrado)
    }
```

**Editar Archivo:** *POST* - ```http://[ip/localhost]:5000/archivo/editar```

* Entrada
```json
    {
        "id_user": String,
        "id_carpeta": String,
        "id_archivo": String,
        "nombre": String
    }
```

* Salida
```json
    {
        "mensaje": String,
        "estado": Int(200 = Ok, 400 = No se pudo obtener usuario de la bd, 403 = Usuario no existe, 401 = Archivo no encontrado, 402 = No se actualizo la informacion de la bd)
    }
```

**Eliminar Archivo:** *POST* - ```http://[ip/localhost]:5000/archivo/eliminar```

* Entrada
```json
    {
        "id_user": String,
        "id_carpeta": String,
        "id_archivo": String
    }
```

* Salida
```json
    {
        "mensaje": String,
        "estado": Int(200 = Ok, 400 = No se pudo obtener usuario de la bd, 403 = Usuario no existe, 401 = Archivo no encontrado)
    }
```

**Obtener Id de Archivo:** *POST* - ```http://[ip/localhost]:5000/archivo/verIdArchivo```

* Entrada
```json
    {
        "id_user": String,
        "id_carpeta": String,
        "nombre": String
    }
```

* Salida
```json
    {
        "mensaje": String,
        "estado": Int(200 = Ok, 400 = No se pudo obtener usuario de la bd, 403 = Usuario no existe, 401 = Archivo no encontrado)
    }
```

**Mover Archivo:** *POST* - ```http://[ip/localhost]:5000/archivo/mover```

* Entrada
```json
    {
        "id_user": String,
        "id_carpeta": String,
        "id_archivo": String,
        "id_carpeta_nueva": String
    }
```

* Salida
```json
    {
        "mensaje": String,
        "estado": Int(200 = Ok, 400 = No se pudo obtener usuario de la bd, 403 = Usuario no existe, 401 = Archivo no encontrado, 402 = Error al actualizar la bd)
    }
```

**Papelera:** *POST* - ```http://[ip/localhost]:5000/archivo/papelera```

* Entrada
```json
    {
        "id_user": String,
        "id_carpeta": String,
        "id_archivo": String,
        "id_carpeta_nueva": String
    }
```

* Salida
```json
    {
        "mensaje": String,
        "estado": Int(200 = Ok, 400 = No se pudo obtener usuario de la bd, 403 = Usuario no existe, 401 = Archivo no encontrado, 402 = Error al actualizar la bd)
    }
```

---

# **Backlog**
## *Endpoints*

**Listado general del backlog:** *GET* - ```http://3.86.240.200:3000/```

* Entrada
```json
    {
        "ruta": "backlog"
    }
```

* Salida
```json
    {
        "data": [{
            "id": "fddf69cd-c416-4a33-ab2e-46c85d96e7e5",
            "salida": {
                "fecha": "7-18-1997",
                "usuario": "tonc2",
                "contrasena": "123",
                "id_user": "5a15fb41ac778871800e8077cbc942d75f6f6e42",
                "nombre": "TOny Gambino",
                "correo": "gambino2@gmail.com"
            },
            "fechaHora": "2021-08-18T17:27:03.411Z",
            "entrada": {
                "ruta": "login",
                "usuario": "tonc2",
                "contrasena": "123"
            },
            "EsError": "No"
        },...]
    }
```