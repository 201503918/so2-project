// LIBRERIAS PARA EL MANEJO DEL SERVER
const express = require('express');
const cors = require('cors');
var router = require('../routes/usuario')

class Server {
    
    constructor(){
        // CONFIGURACIÓN GENERAL DEL SERVIDOR
        this.app = express();
        this.port = 9000;
        this.carpetaPath = "/user";
        
        this.app.use(cors())
        this.app.use(express.json({limit:'10mb'}))
        this.app.use(express.urlencoded({
            extended:false, 
            limit:'10mb'
        }))

        // RUTAS DEL SERVER
        this.routes();
    }

    routes(){
       this.app.use(this.carpetaPath, router);
    }

    listen(){
        this.app.listen( this.port, () => {
            console.log( 'Servidor nodeJS corriendo en puerto: ', this.port );
        });
    }

}

module.exports = Server;