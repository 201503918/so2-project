const express = require('express')
const route = express.Router()
var controller = require('../controller/usuario');
route.post('/editar', controller.editarUsuario);
route.post('/eliminar', controller.eliminarUsuario);
route.post('/autenticar', controller.autenticar);
route.post('/crear', controller.registrarUsuario);
route.post('/login', controller.loginUsuario);
route.post('/validarToken', controller.validateToken);

module.exports = route
