// LIBRERIAS PARA EL MANEJO DEL SERVER
const express = require('express');
const cors = require('cors');
var router = require('../routes/archivo')

class Server {
    
    constructor(){
        // CONFIGURACIÓN GENERAL DEL SERVIDOR
        this.app = express();
        this.port = 5000;
        this.carpetaPath = "/archivo";
        
        this.app.use(cors())
        this.app.use(express.json({limit:'50mb'}))
        this.app.use(express.urlencoded({
            limit: '50mb',
            extended:true, 
            parameterLimit: 50000 
        }))

        // RUTAS DEL SERVER
        this.routes();
    }

    routes(){
       this.app.use(this.carpetaPath, router);
    }

    listen(){
        this.app.listen( this.port, () => {
            console.log( 'Servidor nodeJS corriendo en puerto: ', this.port );
        });
    }

}

module.exports = Server;