const express = require('express')
const route = express.Router()
var controller = require('../controller/archivo')
route.post('/crearArchivo', controller.crearArchivo);
route.post('/ver',controller.verArchivo);
route.post('/editar', controller.editarArchivo);
route.post('/eliminar', controller.eliminarArchivo);
route.post('/verIdArchivo',controller.verIdArchivo);
route.post('/mover',controller.moverArchivo);   
route.post('/papelera', controller.moverPapelera);
route.post('/enviarArchivo', controller.enviarArchivos);

module.exports = route
