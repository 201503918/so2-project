import { Component, OnInit } from "@angular/core";
import { FormsModule, NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import Swal from 'sweetalert2'

// IMPORTACIÓN DEL SERVICIO PETICIONES
import{PeticionesService} from '../../../services/peticiones.service';

declare var M: any;

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
})
export class LoginComponent implements OnInit {
  userName = '';
  password = '';
  tempToken = -1;

  constructor(public ServicePeticiones: PeticionesService,private router: Router) {}

  ngOnInit(): void {
    
  }

  Login(form: NgForm): void {
    if(form.value.NombreUsuario!="" && form.value.Password!=""){
      var infuser={
        "ruta":"login",
        "usuario": this.userName,
        "contrasena": this.password
      };  
      this.validarLogin(infuser,form);
    }
    else{
      Swal.fire({
        icon: 'warning',
        title: 'Campos vacíos',
        text: 'Debe rellenar todos los campos'
      })
    } 
  }

  validarLogin(datos: any, form: NgForm): void {
    let respuestaServer;
    this.ServicePeticiones.postLogin(datos)
    .subscribe(async res => {
      respuestaServer = res;
      if(respuestaServer.message!=null){
        Swal.fire({
          icon: 'error',
          title: 'Credenciales Incorrectas',
          text: 'Las credenciales ingresadas no existen o estan incorrectas, vuelva a intentarlo'
        })
        this.ServicePeticiones.resetForm(form);
      }
      else{
        respuestaServer = this.ServicePeticiones.setUserLoggedIn(respuestaServer);

        if(respuestaServer.auth){
          await this.ServicePeticiones.generateToken(respuestaServer).toPromise().then(
            (res: any) => {
              this.tempToken = res.token;
            }
          );

          const inputValue = ""
          const {value: token} = await Swal.fire({
            title: 'INGRESE TOKEN',
            input: 'text',
            inputLabel: 'Ingrese el token de autentificación',
            inputValue: inputValue,
            showCancelButton: true,
            inputValidator: (value) => {
              if (!value) {
                return 'You need to write something!'
              }
            }
          });

          if(token){

            await this.ServicePeticiones.confirmarToken({ruta: "vToken",secret:respuestaServer.secret, token: token}).toPromise().then(
              (res: any) => {
                if(res.token_valido){
                  Swal.fire({
                    title: 'Credenciales correctas, Bienvenido: \n' + respuestaServer.nombre,
                    width: 600,
                    padding: '3em',
                    background: '#fff url(/assets/img/trees.png)',
                    backdrop: `
                      rgba(0,0,123,0.4)
                      url("/assets/img//nyan-cat.gif")
                      left top
                      no-repeat
                    `
                  }) 
                  this.router.navigate(["/user/miunidad"]);
                  this.ServicePeticiones.resetForm(form);
                }
                else{
                  Swal.fire({
                    icon: 'error',
                    title: 'Credenciales Incorrectas',
                    text: 'El token ingresado no es valido, vuelva a intentarlo'
                  })
                  this.ServicePeticiones.resetForm(form);
                }
              }
            )
          } else{
            Swal.fire({
              icon: 'error',
              title: 'Credenciales Incorrectas',
              text: 'No se ingreso un token, vuelva a intentarlo'
            })
            this.ServicePeticiones.resetForm(form);
          }

        }else {
          Swal.fire({
            title: 'Credenciales correctas, Bienvenido: \n' + respuestaServer.nombre,
            width: 600,
            padding: '3em',
            background: '#fff url(/assets/img/trees.png)',
            backdrop: `
              rgba(0,0,123,0.4)
              url("/assets/img//nyan-cat.gif")
              left top
              no-repeat
            `
          }) 
          this.router.navigate(["/user/miunidad"]);
          this.ServicePeticiones.resetForm(form);
        }
      }
    });
  }

}
