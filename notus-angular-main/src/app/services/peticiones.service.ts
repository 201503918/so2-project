import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, RouterLink } from '@angular/router';
import { stringify } from '@angular/compiler/src/util';
import { FormsModule, NgForm } from '@angular/forms';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { getSingleton } from '../app.module';

@Injectable({
  providedIn: 'root'
})
export class PeticionesService { 
  private url = 'http://35.225.176.70';
  //private url = 'http://localhost:3000';
  private isUserLoggedIn;
  InfJson: any;
  private http: HttpClient;

  constructor(private router: Router) { 
    this.isUserLoggedIn = false;
    this.http = getSingleton();
  }

  //METODO PARA ASIGNAR ALMACENAR AL USUARIO EN EL LOCAL STORAGE
  setUserLoggedIn(User:any) {
    User.id_user = User.id;
    delete User["id"];
    this.isUserLoggedIn = true;
    localStorage.setItem('currentUser', JSON.stringify(User));
    return User;
  }

  //METODO PARA OBTENER LA INFORMACIÓN DEL USUARIO LOGUEADO DEL LOCAL STORAGE
  getUserLoggedIn() {
    //return localStorage.getItem('currentUser');
    return localStorage.getItem('currentUser');
  }

  ObtenerUser()
  {
    
    this.InfJson = localStorage.getItem('currentUser');
    const valjson = JSON.parse(this.InfJson);
    return valjson.usuario;
  }

  Obtenernombre()
  {
    this.InfJson = localStorage.getItem('currentUser');
    const valjson = JSON.parse(this.InfJson);
    return valjson.nombre;
  }

  Obtenernumero()
  {
    this.InfJson = localStorage.getItem('currentUser');
    const valjson = JSON.parse(this.InfJson);
    return valjson.numero;
  }

  
  ObtenerCorreo()
  {
    this.InfJson = localStorage.getItem('currentUser');
    const valjson = JSON.parse(this.InfJson);
    return valjson.correo;
  }


  
  Obtenerfecha()
  {
    this.InfJson = localStorage.getItem('currentUser');
    const valjson = JSON.parse(this.InfJson);
    return valjson.Fecha;
  }

  Obtenercontrasena()
  {
    this.InfJson = localStorage.getItem('currentUser');
    const valjson = JSON.parse(this.InfJson);
    return valjson.contrasena;
  }

  Obtenerauth(){
    this.InfJson = localStorage.getItem('currentUser');
    const valjson = JSON.parse(this.InfJson);

    if(valjson.auth){
      return {
        fa_correo: (valjson.tipo_notificacion == 0 || valjson.tipo_notificacion == 2), 
        fa_mensaje: (valjson.tipo_notificacion == 1 || valjson.tipo_notificacion == 2)
      };
    }

    return {fa_correo: false, fa_mensaje: false};
  }

  confirmarToken(token){
    return this.http.post(this.url + "/user/validarToken",token);
  }

  generateToken(User)
  {
    User.ruta = "autenticar";
    return this.http.post(this.url + "/user/autenticar",User);
  }

  CerrarSesion(){
    var infuser={
      id_user: "",
      usuario: "",
      nombre: "",
      contrasena: "",
      correo: "",
      fecha: ""
    };        
    this.setUserLoggedIn(infuser);
    this.router.navigate(['/']);
  }

  actualizarPagina(){
    const currentRoute = this.router.url;
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigate([currentRoute]); // navigate to same route
        });
  }

  resetForm(form?: NgForm): void {
    if (form){
      form.reset();
    }
  }

// *************************** PETICIONES POST *******************************************
postLogin(User){
  return this.http.post(this.url,User);
}

postRootFolder(User){
  return this.http.post(this.url,User);
}

postCreateFolder(User){
  return this.http.post(this.url,User);
} 

updateFolder(User){
  return this.http.post(this.url,User)
}

deleteFolder(User){
  return this.http.post(this.url,User)
}

}
