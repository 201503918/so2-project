import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { getSingleton } from '../app.module';

@Injectable({
  providedIn: 'root'
})
export class ArchivoService { 
 
  private API_URL = 'http://35.225.176.70'; 
  //private API_URL = 'http://localhost:3000';
  private headers = { 'content-type': 'application/json'}
  private http : HttpClient;

  constructor(private router: Router) {
    this.http = getSingleton();
   }

  actualizarPagina(){
    const currentRoute = this.router.url;
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigate([currentRoute]); // navigate to same route
        });
  }

  downloadFile(link: string): Observable<Blob> {
    return this.http.get(link,{responseType: 'blob'});
  }

  getSelectedFolderUser() {
    return JSON.parse(localStorage.getItem("currentFolder"));
  }

  getSelectedFolder() {
    const folder = JSON.parse(localStorage.getItem("currentFolder"));
    folder.ruta = "dir";

    return this.http.post(`${this.API_URL}`,folder,{
      headers: this.headers
    });
  }

  getSelectedFile() {
    const folder = JSON.parse(localStorage.getItem("currentFile"));
    folder.ruta = "file";

    return this.http.post(`${this.API_URL}`,folder,{
      headers: this.headers
    });
  }

  setSelectedFile(usuario,carpeta,archivo) {
    localStorage.setItem('currentFile',JSON.stringify({id_user: usuario, id_carpeta: carpeta,id_archivo:archivo}));
  }

  createFile(file) {
    return this.http.post(`${this.API_URL}`,file);
  }

  editFile(file) {
    return this.http.post(`${this.API_URL}`,file);
  }

  deleteFile(file) {
    return this.http.post(`${this.API_URL}`,file);
  }

  moveFile(file) {
    return this.http.post(`${this.API_URL}`,file);
  }

  enviar(file) {
    return this.http.post(`${this.API_URL}`,file);
  }


  
}
