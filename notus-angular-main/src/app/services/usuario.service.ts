import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs';
import { getSingleton } from '../app.module';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private API_URL = 'http://35.225.176.70'; 
  //private API_URL = 'http://localhost:3000';
  private headers = { 'content-type': 'application/json'}
  private http: HttpClient;
  
  constructor() { 
    this.http = getSingleton();
  }

  createUser(newUser):Observable<any> {
    
    newUser.ruta = "registrar";

    return this.http.post(`${this.API_URL}`,newUser,{
      headers: this.headers
    });
  }

  editarUsuario(usuario):Observable<any> {
    
    return this.http.post(`${this.API_URL}`,usuario,{
      headers: this.headers
    });
  }

  getFoldersUser(user):Observable<any> {
    return this.http.post<any>(`${this.API_URL}`,user,{
      headers: this.headers
    })
  }


  setSelectedFolder(usuario,carpeta) {
    localStorage.setItem('currentFolder',JSON.stringify({id_user: usuario, id_carpeta: carpeta}));
  }

  getSelectedFolder():Observable<any> {
    const folder = JSON.parse(localStorage.getItem("currentFolder"));
    folder.ruta = "dir";
    return this.http.post<any>(`${this.API_URL}`,folder,{
      headers: this.headers
    });
  }
} 
