require("dotenv").config();
const config=require("./config");
var express = require(`express`);
const axios = require(`axios`).default;
const http = require(`http`);
const port = 3000;
var dateTime = require(`node-datetime`);
var cors = require(`cors`);

var dt = dateTime.create();
dt.format(`d/m/Y H:M:S`);

axios.defaults.timeout = 30000;
axios.defaults.httpAgent = new http.Agent({ keepAlive: true });

var app = express();

var prueba = function(req, res, next){
    let body = req.body;
    if(req.method==`POST`){
       // console.log(body)
        if(req.body.ruta=="login"){
            console.log(body)
            axios
            .post(`${config.USUARIO_URL}/login`,body)
            .then(response =>{
                //console.log(response.data)
                if(response.data.hasOwnProperty(`message`)){
                    if(response.data.message.includes("Error")){
                        
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`,{entrada:body,salida:response.data,EsError:"Si",fechaHora:new Date(dt.now())})
                        .then(response2=>{
                            res.send(response.data)
                            next();
                        })
                    }else{
                        
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`,{entrada:body,salida:response.data,EsError:"No",fechaHora:new Date(dt.now())})
                        .then(response2=>{
                            res.send(response.data)
                            next();
                        })
                    }
                }else{
                   
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`,{entrada:body,salida:response.data,EsError:"No",fechaHora:new Date(dt.now())})
                    .then(response2=>{
                        res.send(response.data)
                        next();
                    })
                    
                }
            })
            
        }else if(req.body.ruta=="registrar"){
            //console.log(body)
            axios
            .post(`${config.USUARIO_URL}/crear`,body)
            .then(response =>{
                console.log(response.data)
                if(response.data.hasOwnProperty(`message`)){
                    if(response.data.message.includes("Error")){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`,{entrada:body,salida:response.data,EsError:"Si",fechaHora:new Date(dt.now())})
                        .then(response2=>{
                            res.send(response.data)
                            next();
                        })
                    }else{
                        
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`,{entrada:body,salida:response.data,EsError:"No",fechaHora:new Date(dt.now())})
                        .then(response2=>{
                            res.send(response.data)
                            next();
                        })
                    }
                }else{
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`,{entrada:body,salida:response.data,EsError:"No",fechaHora:new Date(dt.now())})
                    .then(response2=>{
                        res.send(response.data)
                        next();
                    })
                    
                } 
            })
        }else if(req.body.ruta == "mkdir"){  // CREAR CARPETAS            
            axios
            .post(`${config.MICROSERVICE_CARPETA_URL}/create`, body)
            .then(response => {
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(
                            response2 => {
                                res.send(response.data);
                                next();
                        });
                    }
                } else {
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(
                        response2 => {
                            res.send(response.data);
                            next();
                    });
                }
            });
        }else if(req.body.ruta == "rmdir"){  // ELIMINIAR CARPETA
            axios
            .post(`${config.MICROSERVICE_CARPETA_URL}/delete`, body) 
            .then(response => {
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(
                            response2 => {
                                res.send(response.data);
                                next();
                        });
                    }
                } else {
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(
                        response2 => {
                            res.send(response.data);
                            next();
                    });
                }
            });
        }else if(req.body.ruta == "root"){  // VER CARPETAS DESDE EL ROOT
            axios
            .post(`${config.MICROSERVICE_CARPETA_URL}/root`, body)
            .then(response => {
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            res.send(response.data);
                            next();
                        });  
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(
                            response2 => {
                                res.send(response.data);
                                next();
                        });
                    }
                } else {
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(
                        response2 => {
                            res.send(response.data);
                            next();
                    });
                }
            });
        }else if(req.body.ruta == "dir"){  // VER CARPETA POR ID
            axios
            .post(`${config.MICROSERVICE_CARPETA_URL}/read`, body)
            .then(response => {
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(
                            response2 => {
                                res.send(response.data);
                                next();
                        });
                    }
                } else {
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(
                        response2 => {
                            res.send(response.data);
                            next();
                    });
                }
            });
        }else if(req.body.ruta == "mvdir"){  // ACTUALIZAR CARPETA
            axios
            .post(`${config.MICROSERVICE_CARPETA_URL}/update`, body)
            .then(response => {
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(
                            response2 => {
                                res.send(response.data);
                                next();
                        });
                    }
                } else {
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/carpetas`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(
                        response2 => {
                            res.send(response.data);
                            next();
                    });
                }
            });
        }else if(req.body.ruta == "backlog"){   // LISTADO GENERAL DEL BACKLOG
            axios
            .get(`${config.MICROSERVICE_BACKLOG_URL}/logs`)
            .then(response => {
                res.send(response.data);
                next();                
            });
        }else if(req.body.ruta == "eliminar"){// ELIMINAR USUARIO
         
            axios
            .post(`${config.MICROSERVICE_USUARIO_URL}/eliminar`,body)
            .then(response =>{
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response2.data);
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response2.data);
                            res.send(response.data);
                            next();
                        });
                    }
                }else{
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(response2 => {
                        console.log(response2.data);
                        res.send(response.data);
                        next();
                    });
                }
            })
        }else if(req.body.ruta == "editar"){ //EDITAR USUARIO
            axios
            .post(`${config.MICROSERVICE_USUARIO_URL}/editar`,body)
            .then(response =>{
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response2.data);
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response2.data);
                            res.send(response.data);
                            next();
                        });
                    }
                }else{
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(response2 => {
                        console.log(response2.data);
                        res.send(response.data);
                        next();
                    });
                }
                
            })
        }else if(req.body.ruta == "cat"){ //CREAR ARCHIVO
            axios
            .post(`${config.ARCHIVO_URL}/crearArchivo`, body)
            .then(response => {
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response2.data);
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response2.data);
                            res.send(response.data);
                            next();
                        });
                    }
                }else{
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(response2 => {
                        console.log(response2.data);
                        res.send(response.data);
                        next();
                    });
                }
            })
        }else if(req.body.ruta == "file"){ //VER ARCHIVOS DE UNA CARPETA
            axios
            .post(`${config.ARCHIVO_URL}/ver`, body)
            .then(response => {
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response2.data);
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response.data);
                            res.send(response.data);
                            next();
                        });
                    }
                }else{
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(response2 => {
                        console.log(response.data);
                        res.send(response.data);
                        next();
                    });
                }
            })

        }else if(req.body.ruta == "mvfile"){// EDITAR ARCHIVOS
            axios
            .post(`${config.ARCHIVO_URL}/editar`, body)
            .then(response => {
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response2.data);
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response.data);
                            res.send(response.data);
                            next();
                        });
                    }
                }else{
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(response2 => {
                        console.log(response.data);
                        res.send(response.data);
                        next();
                    });
                }
            })
        }else if(req.body.ruta == "rmfile"){//ELIMINAR ARCHIVO
            axios
            .post(`${config.ARCHIVO_URL}/eliminar`, body)
            .then(response => {
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response2.data);
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response.data);
                            res.send(response.data);
                            next();
                        });
                    }
                }else{
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(response2 => {
                        console.log(response.data);
                        res.send(response.data);
                        next();
                    });
                }
            })
        }else if(req.body.ruta == "idfile"){//OBTENER EL ID DE ARCHIVO
            axios
            .post(`${config.ARCHIVO_URL}/verIdArchivo`, body)
            .then(response => {
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response2.data);
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response.data);
                            res.send(response.data);
                            next();
                        });
                    }
                }else{
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(response2 => {
                        console.log(response.data);
                        res.send(response.data);
                        next();
                    });
                }
            })
        }else if(req.body.ruta == "movfile"){//MOVER ARCHIVO
            axios
            .post(`${config.ARCHIVO_URL}/mover`, body)
            .then(response => {
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response2.data);
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response.data);
                            res.send(response.data);
                            next();
                        });
                    }
                }else{
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(response2 => {
                        console.log(response.data);
                        res.send(response.data);
                        next();
                    });
                }
            })
        }else if(req.body.ruta == "autenticar"){//EVIAR EMAIL &  SMS DE AUTENTICACION
            axios
            .post(`${config.MICROSERVICE_USUARIO_URL}/autenticar`, body)
            .then(response => {
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response2.data);
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            console.log(response.data);
                            res.send(response.data);
                            next();
                        });
                    }
                }else{
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(response2 => {
                        console.log(response.data);
                        res.send(response.data);
                        next();
                    });
                }
            })
        }else if(req.body.ruta == "papelera"){// PAPELERA
            axios
            .post(`${config.ARCHIVO_URL}/papelera`, body)
            .then(response => {
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(
                            response2 => {
                                res.send(response.data);
                                next();
                        });
                    }
                } else {
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(
                        response2 => {
                            res.send(response.data);
                            next();
                    });
                }
            });
        }else if(req.body.ruta == "vToken"){// VALIDAR TOKEN
            console.log(body)
            axios
            .post(`${config.USUARIO_URL}/validarToken`,body)
            .then(response =>{
                //console.log(response.data)
                if(response.data.hasOwnProperty(`message`)){
                    if(response.data.message.includes("Error")){
                        
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`,{entrada:body,salida:response.data,EsError:"Si",fechaHora:new Date(dt.now())})
                        .then(response2=>{
                            res.send(response.data)
                            next();
                        })
                    }else{
                        
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`,{entrada:body,salida:response.data,EsError:"No",fechaHora:new Date(dt.now())})
                        .then(response2=>{
                            res.send(response.data)
                            next();
                        })
                    }
                }else{
                   
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/usuarios`,{entrada:body,salida:response.data,EsError:"No",fechaHora:new Date(dt.now())})
                    .then(response2=>{
                        res.send(response.data)
                        next();
                    })
                    
                }
            })
        }else if(req.body.ruta == "enviar"){//ENVIAR ARCHIVO A CORREO
            axios
            .post(`${config.ARCHIVO_URL}/enviarArchivo`, body)
            .then(response => {
                if(response.data.hasOwnProperty(`estado`)){
                    if(response.data.estado == 200){
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "No",
                            fechaHora:new Date(dt.now())
                        })
                        .then(response2 => {
                            res.send(response.data);
                            next();
                        });
                    }else{
                        axios
                        .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                            entrada: body,
                            salida: response.data,
                            EsError: "Si",
                            fechaHora:new Date(dt.now())
                        })
                        .then(
                            response2 => {
                                res.send(response.data);
                                next();
                        });
                    }
                } else {
                    axios
                    .post(`${config.MICROSERVICE_BACKLOG_URL}/archivos`, {
                        entrada: body,
                        salida: response.data,
                        EsError: "Si",
                        fechaHora:new Date(dt.now())
                    })
                    .then(
                        response2 => {
                            res.send(response.data);
                            next();
                    });
                }
            });
        }
    }
}

app.use(express.json());
app.use(cors({origin: true, optionsSuccessStatus: 200}));
app.use(express.urlencoded({extended:true, limit:`10mb`}))
app.use(prueba);



app.listen(port, () => {
    console.log(`Middleware escuchando en el puerto: ${port}`)
  });
