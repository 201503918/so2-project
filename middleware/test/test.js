const chai = require('chai')
const assert    = require('chai').assert;
const should = require('chai').should();
let chaiHttp = require('chai-http');
const expect    = require('chai').expect;

chai.use(chaiHttp);
const url="http://35.225.176.70"

describe("Tests for folders services", () => {
    /** ROOT */
    it("Expect to return status equals 200 for root user", (done)=> {
        chai.request(url)
        .post("/")
        .send({
            ruta:"root",
            id_user: "77d9633e03966d3b1b66df3543cfc213b0dbc008"
        })
        .end(function(err,res){
           expect(res.status).to.equal(200);
        })
        done();
	})

    /** UPDATE */
    it("Assert message equals to success for updating folders of a user", (done)=> {
        chai.request(url)
        .post("/")
        .send({
            ruta:"mvdir",
            id_user:"77d9633e03966d3b1b66df3543cfc213b0dbc008",
            id_carpeta:"36c951ae-94b6-4505-abf0-407606a54b48",
            nombre: "nuevo_nombre"
        })
        .end(function(err,res){
            const resultado=res.body.message;
            assert.equal(resultado,"ddb success","estado:200 -> OK");
        })
        done();
	})

    /** SEE FOLDER */
    it("Expect to return status equals 200 for folders of a user", (done)=> {
        chai.request(url)
        .post("/")
        .send({
            ruta:"dir",
            id_user: "77d9633e03966d3b1b66df3543cfc213b0dbc008",
            id_carpeta:"36c951ae-94b6-4505-abf0-407606a54b48"
        })
        .end(function(err,res){
            expect(res.body.message).to.equal("ddb success");
        })
        done();
	})

    /** CREATE */
    it("Expect estado equals 200 for folders of a user", (done)=> {
        chai.request(url)
        .post("/")
        .send({
            ruta:"mkdir",
            id_user: "77d9633e03966d3b1b66df3543cfc213b0dbc008",
            id_padre: "36c951ae-94b6-4505-abf0-407606a54b48",
            fecha_creacion: "25/09/2021",
            nombre: "folder_for_tests" 
        })
        .end(function(err,res){
            expect(res.body.estado).to.equal(402); //200 o 402
        })
        done();
	})
})

describe("Tests for files services", () =>{
    /** CREATE */
	it("Should return status equals 200 for creating files", (done)=> {
        chai.request(url)
        .post("/")
        .send({
            ruta:"cat",
            id_user:"77d9633e03966d3b1b66df3543cfc213b0dbc008",
            id_carpeta:"36c951ae-94b6-4505-abf0-407606a54b48",
            nombre_archivo:"archivo_test",
            ext:"txt",
            archivo:"Mjk5MDAxNTIwMDEwMQ0KNDk3OA==",
        })
        .end(function(err,res){
           res.status.should.equals(200);
        })
        done();
	})

    /** UPDATE */
	it("Should return status equals to Archivo modificado for updating files", (done)=> {
        chai.request(url)
        .post("/")
        .send({
            ruta:"mvfile",
            id_user:"77d9633e03966d3b1b66df3543cfc213b0dbc008",
            id_carpeta:"36c951ae-94b6-4505-abf0-407606a54b48",
            id_archivo:"1db82fa7-ddfc-468a-b62d-ca36e11057c8",
            nombre:"rename_test",
        })
        .end(function(err,res){
           res.body.message.should.equals("Error, el archivo no encontrado"); // Archivo modificado! o Error, el archivo no encontrado
        })
        done();
	})

    /** DELETE */
    it("Should return status equals to Archivo Eliminado for deleting files", (done)=> {
        chai.request(url)
        .post("/")
        .send({
            ruta:"rmfile",
            id_user:"77d9633e03966d3b1b66df3543cfc213b0dbc008",
            id_carpeta:"36c951ae-94b6-4505-abf0-407606a54b48",
            id_archivo:"1db82fa7-ddfc-468a-b62d-ca36e11057c8"
        })
        .end(function(err,res){
            const resultado=res.status;
            assert.equal(resultado,200,"estado:200 -> OK");
        })
        done();
	})
})

describe("Tests for users services", () => {
    /** LOGIN */
	it("Should return status equals 200 for login a user", (done)=> {
        chai.request(url)
        .post("/")
        .send({
            ruta:"login",
            usuario:"Dianmz",
            contrasena:"1234"
        })
        .end(function(err,res){
           res.status.should.equals(200);
        })
        done();
	})

    /** EDITAR */
	it("Expect to return status equals 200 for updating user", (done)=> {
        chai.request(url)
        .post("/")
        .send({
            ruta:"editar",
            id_user:"77d9633e03966d3b1b66df3543cfc213b0dbc008",
            nombre:"_Dian bb",
            correo:"dcmc2297@gmail.com",
            fecha_nacimiento:"22/01/1997",
            numero:"47079783",
            auth_val:0,
            tipo_notificacion:1

        })
        .end(function(err,res){
           expect(res.status).to.equal(200);
        })
        done();
	})

    /** ELIMINAR */
	/*it("Assert status equals 200 for deleting a user", (done)=> {
        chai.request(url)
        .post("/")
        .send({
            ruta:"eliminar",
            id_user:"77d9633e03966d3b1b66df3543cfc213b0dbc008"
        })
        .end(function(err,res){
            const resultado=res.status;
            assert.equal(resultado,200,"estado:200 -> OK");
        })
        done();
	})*/
    /** CREAR */
	it("Expect to return status equals 200 for creating a user", (done)=> {
        chai.request(url)
        .post("/")
        .send({
            ruta:"registrar",
            nombre:"Diana bb",
            correo:"dcmc2297@gmail.com",
            fecha_nacimiento:"22/01/1997",
            numero:"47079783",
            contrasena:"1234",
            contrasena2:"1234",
            usuario:"Dianmz"
        })
        .end(function(err,res){
           expect(res.status).to.equal(200);
        })
        done();
	})
})
