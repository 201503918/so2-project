# Carpetas
### Endpoints

Crear carpeta POST ```[ip]/api/carpetas/create```

* Entrada
```
    {
        "id_user": Int,
        "fecha_creacion": "",
        "nombre": "",
        "id_padre": Int,
    }
```

* Salida
```
    {
        "mensaje": "",
        "estado": Int(200 = Ok, 403 = Usuario no encontrado, 404 = Ruta no encontrada, 400 = DB Fallo, 402 = Carpeta duplicada),
    }
```

Eliminar carpeta DELETE ```[ip]/api/carpetas/delete```

* Entrada
```
    {
        "id_user": Int,
        "id_carpeta": Int,
    }
```

* Salida
```
    {
        "mensaje": "",
        "estado": Int(200 = Ok, 403 = Usuario no encontrado, 404 = Carpeta no encontrada, 400 = DB Fallo),
    }
```

Ver root carpetas POST ```[ip]/api/carpetas/root```

* Entrada
```
    {
        "id_user": Int
    }
```

* Salida
```
    {
        "mensaje": "",
        "carpetas": [{
            "id": Int,
            "nombre": "",
            "fecha_creacion": "",
            "carpetas": [],
            "archivos": []
        }]
        "estado": Int(200 = Ok, 403 = Usuario no encontrado, 400 = DB Fallo),
    }
```

Ver carpeta por id POST ```[ip]/api/carpetas/read```

* Entrada
```
    {
        "id_user": Int,
        "id_carpeta": Int
    }
```

* Salida
```
    {
        "mensaje": "",
        "carpetas": [{
            "id": Int,
            "nombre": "",
            "fecha_creacion": "",
            "carpetas": [],
            "archivos": []
        }]
        "estado": Int(200 = Ok, 403 = Usuario no encontrado, 404 = Carpeta no encontrada, 400 = DB Fallo),
    }
```

Actualizar carpeta id POST ```[ip]/api/carpetas/update```

* Entrada
```
    {
        "id_user": Int,
        "id_carpeta": Int,
        "nombre": ""
    }
```

* Salida
```
    {
        "mensaje": "",
        "estado": Int(200 = Ok, 403 = Usuario no encontrado, 404 = Carpeta no encontrada, 400 = DB Fallo, 402 = Carpeta duplicada)
    }
```