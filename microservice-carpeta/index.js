// INICIAR LA CONFIGURACIÓN DEL ARCHIVO .ENV
require('dotenv').config();

const Server = require('./models/server');

//INSTANCIA DE LA CLASE SERVER PARA LEVANTARLO
const server = new Server();
server.listen();