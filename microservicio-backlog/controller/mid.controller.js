const { Response, Request } = require('express');
const aws_keys = require('../models/config');
const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient(aws_keys.dynamodb);
const { v4: uuidv4 } = require('uuid');
const crypto = require('crypto');
const { send } = require('process');

const registroUsuario = (req = Request, res = Response) => {
    let body = req.body;
    body['id']=uuidv4();
    var params = {
        TableName: aws_keys.aws_table_name,
        Item: body
    };
    ddb.put(params, function (err, data) {
        if (err) { 
            res.send({
                'message': 'Error',
            });                  
        } else {  
            console.log(body);
            res.send(body)        
        }
    }); 
}

const registroCarpeta = (req = Request, res = Response) => {
    let body = req.body;
    body['id'] = uuidv4();
    console.log(body);
    var params = {
        TableName: aws_keys.aws_table_name,
        Item: body
    };
    ddb.put(params, function (err, data) {
        if (err) {
            res.send({
                'message': 'Error'
            });
        } else {
            res.send(body);
        }
    });
}

const registroArchivos = (req = Request, res = Response) => {
    let body = req.body;
    body['id'] = uuidv4();
    console.log(body);
    var params = {
        TableName: aws_keys.aws_table_name,
        Item: body
    };
    ddb.put(params, function (err, data) {
        if (err) {
            res.send({
                'message': 'Error'
            });
        } else {
            res.send(body);
        }
    });
}

const verRegistros = async (req = Request, res = Response) => {
    let params = {
        TableName: aws_keys.aws_table_name
    };
    let data = await dbRead(params);
    
    res.send({
        data
    });
}

async function dbRead(params) {
    let promise = ddb.scan(params).promise();
    let result = await promise;
    let data = result.Items;
    if (result.LastEvaluatedKey) {
        params.ExclusiveStartKey = result.LastEvaluatedKey;
        data = data.concat(await dbRead(params));
    }
    return data;
}

module.exports = {
    registroUsuario,
    registroCarpeta,
    registroArchivos,
    verRegistros
}